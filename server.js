const Vigenere = require('caesar-salad').Vigenere;
const express = require('express');


const app = express();
const port = 8000;
const password = 'password';

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});

app.get('/encode/:code', (req, res) => {
    const encode = Vigenere.Cipher(password).crypt(req.params.code);

    res.send(encode);
});

app.get('/decode/:code', (req, res) => {
    const decode = Vigenere.Decipher(password).crypt(req.params.code);
    res.send(decode);
});

app.listen(port, () => {
    console.log('Port: ' + port);
});
